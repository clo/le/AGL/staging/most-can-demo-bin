#!/bin/bash

if [ ! "$(whoami)" = "root" ]
then
	echo Warning you are not root, expect problems!
fi

if [ -d /home/pi/appsteam/trunk/LinuxDriver ]
then
	cd /home/pi/appsteam/trunk/LinuxDriver
	echo Switched to folder:$(pwd)
else
	echo Searching driver in folder:$(pwd)
fi

#unload MOST Kernel modules
lsmod | grep "^mxc_mlb\>" > /dev/null &&
	rmmod mxc_mlb && echo "unloaded mxc_mlb"

lsmod | grep "^hdm_dim2_mx6q\>" > /dev/null &&
	rmmod hdm_dim2_mx6q && echo "unloaded hdm_dim2_mx6q"

lsmod | grep "^hdm_dim2\>" > /dev/null &&
	rmmod hdm_dim2 && echo "unloaded hdm_dim2"

lsmod | grep "^hdm_usb\>" > /dev/null &&
	rmmod hdm_usb && echo "unloaded hdm_usb"

lsmod | grep "^aim_sound\>" > /dev/null &&
	rmmod aim_sound && echo "unloaded aim_sound"

lsmod | grep "^aim_v4l2\>" > /dev/null &&
	rmmod aim_v4l2 && echo "unloaded aim_v4l2"

lsmod | grep "^aim_cdev\>" > /dev/null &&
	rmmod aim_cdev && echo "unloaded aim_cdev"

lsmod | grep "^aim_network\>" > /dev/null &&
	rmmod aim_network && echo "unloaded aim_network"

lsmod | grep "^mostcore\>" > /dev/null &&
	rmmod mostcore && echo "unloaded mostcore"

echo
#load V4L support
lsmod | grep "^media\>" ||
  insmod /lib/modules/$(uname -r)/kernel/drivers/media/media.ko

lsmod | grep "^videodev\>" ||
  insmod /lib/modules/$(uname -r)/kernel/drivers/media/v4l2-core/videodev.ko

#load MOST Kernel modules
insmod mostcore.ko && echo "loaded mostcore"
insmod aim_cdev.ko && echo "loaded aim_cdev"

[ -e aim_network.ko ] &&
	insmod aim_network.ko && echo "loaded aim_network"

[ -e aim_sound.ko ] &&
	insmod aim_sound.ko && echo "loaded sound"	

[ -e aim_v4l2.ko ] &&
	insmod aim_v4l2.ko && echo "loaded v4l2"

[ -e hdm_usb.ko ] &&
	insmod hdm_usb.ko && echo "loaded hdm_usb"

[ -e hdm_dim2.ko ] &&
	insmod hdm_dim2.ko clock_speed=4096fs fcnt=3 && echo "loaded hdm_dim2"

[ -e hdm_dim2_mx6q.ko ] &&
	insmod hdm_dim2_mx6q.ko && echo "loaded hdm_dim2_mx6q"

DEVS=/sys/devices/virtual/most/mostcore/devices
AIMS=/sys/devices/virtual/most/mostcore/aims

xset()
{
	VAL=$1
	FILE=$2

	#echo "$VAL > $FILE"
	echo "$VAL" > $FILE
}

SetAsyncChannel()
{
	MDEV=mdev$1
	CH_NAME=$2
	DIR=dir_$3
	BUFSIZE=$4
	
	CHANNEL=$DEVS/$MDEV/$CH_NAME

	[ -e $CHANNEL/set_buffer_size ] || return 1

	[ "$(cat $CHANNEL/set_buffer_size)" = "0" ] || return 2

	echo "Set Ethernet $MDEV channel: $CH_NAME"

	xset async    $CHANNEL/set_datatype
	xset $DIR     $CHANNEL/set_direction
	xset 32       $CHANNEL/set_number_of_buffers
	xset $BUFSIZE $CHANNEL/set_buffer_size

	xset $MDEV:$CH_NAME: $AIMS/networking/add_link
	return 0
}

StartupEth()
{
	METH=meth$1
	IFC_ALL=$2
	IFC_UP=$3
	
	#if device not exist return
	echo "$IFC_ALL" | grep -q $METH || return 1
	
	#if the interface is down, start it
	if [ -z "`echo "$IFC_UP" | grep $METH`" ]
	then
		echo Starting up $METH
		ifconfig $METH up
		return 2
	fi

	#if the IP is already set, we have nothing to do anymore
	ifconfig $METH |grep -q "inet addr:" && return 3

	MAC=`ifconfig -a $METH | awk '/HWaddr/{ print $5}'`
	#if MAC address has not been set yet return	
	[ -z "$MAC" ] && return 4
	[ $MAC = "FF:FF:FF:FF:FF:FF" ] && return 5
	[ $MAC = "ff:ff:ff:ff:ff:ff" ] && return 5
	[ $MAC = "00:00:00:00:00:00" ] && return 6

	m0=$(echo $((16#${MAC:0:2})))
	m1=$(echo $((16#${MAC:3:2})))
	m2=$(echo $((16#${MAC:6:2})))
	m3=$(echo $((16#${MAC:9:2})))
	m4=$(echo $((16#${MAC:12:2})))
	m5=$(echo $((16#${MAC:15:2})))

	IP=10.0.$m2.$((1+$m5))
	echo Setting IP address $IP for device $METH MAC:$m0-$m1-$m2-$m3-$m4-$m5
	ifconfig $METH $IP netmask 255.255.255.0
	return 0
}

##########################
## USER ADJUSTABLE AREA ##
##########################
while :
do
	for i in 0 1 2 3 4 5 6 7 8 9
	do
#USB attached INICs:
#MEP
		SetAsyncChannel $i ep8e rx 1548 &&
		SetAsyncChannel $i ep0e tx 1548
#MLB attached INICs:
#MEP
		SetAsyncChannel $i ca6 rx 1548 &&
		SetAsyncChannel $i ca8 tx 1548
	done
	IFC_ALL="$(ifconfig -a)"
	IFC_UP="$(ifconfig)"
	for i in 0 1 2 3 4 5 6 7 8 9
	do
		StartupEth $i "$IFC_ALL" "$IFC_UP"
	done
	sleep 3
done

